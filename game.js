
var turns = 7; //sets the number of turns left to 7 - maximum number of tries allowed
var winningNumber; //randomly generated number
var min; //used for generating the random number
var max; //used for generating the random number
var response; //number guessed by the user

//hides the "Try Again" button so that it isn't available until the game is over
document.getElementById("tryAgain").style.display = "none";

//creates a random number inclusive of minimum and maximum
winningNumber = getRandomInt(1, 100);

//function is called when Submit button is pushed
function runProgram() {
  
  if (turns > 0) { //only runs if there are turns left
    
    //gets the number that the user entered
    var char = document.getElementById("guessNumInput").value;
    response = Number(char);
  
    //in case of invalid response
    if (response < 1 || response > 100 || response === NaN) { 
      window.alert("The response you entered was not between 1 and 100.");
      document.getElementById("guessNumInput").value=''; //clears number field
      return; //immediately exits the function and gives control back to the user
    }
    
    //if response is valid
    if (response === winningNumber) { //if user has guessed the winning number
      turns--; //counts the winning turn as a turn
      if (turns !== 6) { //formats response correctly if user won in multiple turns
         document.getElementById("feedback").innerHTML += "You won in " + (7 - turns) + " turns!";
      }
      if (turns === 6) { //formats reponse correctly if user won in one turn
         document.getElementById("feedback").innerHTML += "You won in 1 turn!";
      }
      //sets display of number of turns left
      document.getElementById("turnsLeft").innerHTML = "&nbsp" + turns;
       
      youWin(); //runs the youWin function
        
      turns = 0; //sets internal count of turns to 0, so program is unresponsive until game is reset
      return; //immediately ends program
      
    } else { //if haven't won yet
      
        if (response < winningNumber) { //if winning number is greater than user guess
            document.getElementById("feedback").innerHTML += "Greater than " + response + "<br>";
        } else { //if winning number is less than user guess
                document.getElementById("feedback").innerHTML += "Less than " + response + "<br>";
        } // end of if less than or greater than
      
    } //end of if winning or not winning       
   
    turns--;
    if (turns === 0) { //if user runs out of turns and loses
      document.getElementById("feedback").innerHTML += "The winning number was " + winningNumber + ".";
      youLose(); //runs youLose function
    } //end of if out of turns
    
    document.getElementById("turnsLeft").innerHTML = "&nbsp" + turns; //updates number of turns left
    document.getElementById("guessNumInput").value=''; //erases number field
    
  } //end of if turns greater than zero
   
} //end of runProgram function

//creates a random number inclusive of maximum and minimum
function getRandomInt(min, max) {
    min = Math.ceil(min);
    max = Math.floor(max);
    return Math.floor(Math.random() * (max - min + 1)) + min; 
}

//reloads screen so that all number reset to play again
//this function doesn't work in codePen unless you are in Debug mode
function resetProgram() { 
  location.reload(true);
  return false;
}

//runs when user runs out of turns
function youLose() {
  document.getElementById("tryAgain").style.display = "block"; //displays "Try Again" button
  //displays "You Lose" graphic in popup window
  window.open("https://i.ibb.co/JjT02RH/you-lose-two.jpg", "_blank", "toolbar=no,scrollbars=no,resizable=no,top=100,left=300,width=700,height=400");
}
  
//runs when user wins
function youWin() {
  document.getElementById("tryAgain").style.display = "block"; //displays "Try Again" button
  //displays "You Win" graphic in popup window
  window.open("https://i.ibb.co/cQh2Vp3/you-won-two.jpg", "_blank", "toolbar=no,scrollbars=no,resizable=no,top=100,left=300,width=700,height=400");
}


